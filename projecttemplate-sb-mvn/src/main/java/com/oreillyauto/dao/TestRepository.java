package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.TestRepositoryCustom;
import com.oreillyauto.domain.Test;

public interface TestRepository extends CrudRepository<Test, Integer>, TestRepositoryCustom {
	// Spring Data abstract methods go here

}
