package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Student;

public interface UniversityService {
    public Student getStudentById(String id) throws Exception;
    public List<Student> getStudents();
    public void saveStudent(Student student);
    public void deleteStudentById(Student student);
}
