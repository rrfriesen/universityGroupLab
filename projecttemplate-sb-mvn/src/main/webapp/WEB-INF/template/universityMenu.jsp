<%@ include file="/WEB-INF/layouts/include.jsp"%>
<nav class="navbar bg-light">
	<ul class="nav nav-pills flex-column">
<%-- 		<li>
		    <a class="${active eq 'add' ? 'active nav-link' : 'nav-link'}" 
		       href="<c:url value='/carparts/partnumber' />">
		    	Add Part Number
		    </a>
		</li> --%>
		<li class="nav-item">
			<a class="${active eq 'carpartManager' ? 'active nav-link' : 'nav-link'}" 
			href="<c:url value='/university/newStudent'/>">
				New Student
			</a>
		</li>
		<li class="nav-item">
			<a class="${active eq 'H4666ST' ? 'active nav-link' : 'nav-link'}" 
			   href="<c:url value='/university/courseReport'/>">
				Course Report
			</a>
		</li>
	</ul>
</nav>