DROP TABLE IF EXISTS main_table;
DROP TABLE IF EXISTS lookup_table;

CREATE TABLE lookup_table(
	course_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    course_name VARCHAR(128),
    professor VARCHAR(128),
    days VARCHAR(128),
    duration INTEGER
);

CREATE TABLE main_table(
	tx_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    course_id INTEGER,
    first_name VARCHAR(128),
    last_name VARCHAR(128),
    enroll_date TIMESTAMP,
    FOREIGN KEY (course_id) REFERENCES lookup_table(course_id)
);

INSERT INTO lookup_table(course_name, professor, days, duration) VALUES
('Intro to CS', 'Friesen', 'T/TH', 90),
('Biology', 'Kamran', 'M/W/F', 75);